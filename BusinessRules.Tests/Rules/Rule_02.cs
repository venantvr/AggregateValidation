using BusinessRules.Engine;
using BusinessRules.Engine.Handlers;
using BusinessRules.Enums;
using BusinessRules.Template;
using BusinessRules.Tests.Entities;

namespace BusinessRules.Tests.Rules
{
    // ReSharper disable once InconsistentNaming
    public class Rule_02 : AbstractBusinessRule<Entity_02>
    {
        public override string Key => AggregateRoot.Id.ToString();

        [BusinessRule("Test : {0}", RuleLifeCycle.Invariant, CriticityLevel.Error, typeof (ConsoleHandler))]
        public bool Check_01()
        {
            return false;
        }
    }
}