using BusinessRules.Engine;
using BusinessRules.Enums;
using BusinessRules.Template;
using BusinessRules.Tests.Entities;
using BusinessRules.Tests.Handlers;

namespace BusinessRules.Tests.Rules
{
    // ReSharper disable once InconsistentNaming
    public class Rule_03 : AbstractBusinessRule<Entity_03>
    {
        private readonly Entity_03 _aggregateRoot;

        //public Rule_03()
        //{
        //}

        public Rule_03(Entity_03 aggregateRoot)
        {
            _aggregateRoot = aggregateRoot;
        }

        public override string Key => _aggregateRoot.Id.ToString();

        [BusinessRule("Test : {0}", RuleLifeCycle.Invariant, CriticityLevel.Error, typeof (SnapshotHandler))]
        public bool Check_01()
        {
            return false;
        }

        [BusinessRule("Test : {0}", RuleLifeCycle.Invariant, CriticityLevel.Error, typeof (SnapshotHandler))]
        public bool Check_02()
        {
            return false;
        }
    }
}