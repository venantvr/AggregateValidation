using System;
using System.Collections.Generic;
using System.Linq;
using BusinessRules.Engine.Handlers;
using BusinessRules.Enums;
using BusinessRules.Tests.Entities;
using BusinessRules.Tests.Handlers;
using BusinessRules.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessRules.Tests
{
    [TestClass]
    public class BusinessContractTest
    {
        [TestMethod]
        //[ExpectedException(typeof(AggregateException))]
        public void TestBusinessRuleEngine_01()
        {
            Exception exception = null;

            try
            {
                var aggregateRoot = new SampleAggregateRoot { Price = 0.0M };

                BusinessContracts.Run<BusinessTransaction, SampleAggregateRoot>(aggregateRoot, ar => ar.Process());
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            Assert.IsTrue(exception != null);
            Assert.IsTrue(exception.InnerException != null && exception.InnerException.GetType() == typeof (BusinessException));
            Assert.IsTrue(exception.Message == @"One or more errors occurred.");
            Assert.IsTrue(exception.InnerException.Message == @"Error - The price of product 0 should be positive.");
        }

        [TestMethod]
        public void TestBusinessRuleEngine_02()
        {
            var aggregateRoot = new SampleAggregateRoot { Price = 100.0M };

            BusinessContracts.Run<BusinessTransaction, SampleAggregateRoot>(aggregateRoot, a => a.Process());

            Assert.IsTrue(aggregateRoot.Price == 101.0M);
        }

        [TestMethod]
        public void TestBusinessRuleEngine_03()
        {
            var aggregateRoot = new SampleAggregateRoot { Price = 100.0M };

            BusinessContracts.Requires(aggregateRoot, RuleLifeCycle.Invariant, () => new ExceptionHandler());
            BusinessContracts.Requires(aggregateRoot, RuleLifeCycle.PreCondition, () => new ExceptionHandler());

            Assert.IsTrue(aggregateRoot.Price == 100.0M);
        }

        [TestMethod]
        public void TestBusinessRuleEngine_04()
        {
            var aggregateRoot = new SampleAggregateRoot { Price = 0.0M };

            var messages = new List<string>();

            BusinessContracts.Requires(aggregateRoot, RuleLifeCycle.Invariant, () => new TestHandler(messages));
            BusinessContracts.Requires(aggregateRoot, RuleLifeCycle.PreCondition, () => new TestHandler(messages));

            Assert.IsTrue(messages.Count == 2);
            Assert.IsTrue(messages.All(m => m == @"The price of product 0 should be positive."));
        }
    }
}