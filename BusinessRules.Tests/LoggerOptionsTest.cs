﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessRules.Engine;
using BusinessRules.Engine.Handlers;
using BusinessRules.Engine.Logger;
using BusinessRules.Engine.Template;
using BusinessRules.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessRules.Tests
{
    [TestClass]
    public class LoggerOptionsTest
    {
        [TestMethod]
        public void TestLoggers_01()
        {
            using (new LoggerOptions(null))
            {
                using (new LoggerOptions(null))
                {
                    Assert.IsTrue(!LoggerOptions.Current.Loggers.Any());
                }
            }
        }

        [TestMethod]
        public void TestLoggers_02()
        {
            var console = new List<Handler> { new ConsoleHandler() };
            var mail = new List<Handler> { new MailHandler() };

            using (new LoggerOptions(console.AsReadOnly()))
            {
                Assert.IsTrue(LoggerOptions.Current.Loggers.Any());
                Assert.IsTrue(LoggerOptions.Current.Loggers.First().GetType() == typeof (ConsoleHandler));

                using (new LoggerOptions(mail.AsReadOnly()))
                {
                    Assert.IsTrue(LoggerOptions.Current.Loggers.Any());
                    Assert.IsTrue(LoggerOptions.Current.Loggers.First().GetType() == typeof (MailHandler));
                }

                Assert.IsTrue(LoggerOptions.Current.Loggers.Any());
                Assert.IsTrue(LoggerOptions.Current.Loggers.First().GetType() == typeof (ConsoleHandler));
            }
        }

        [TestMethod]
        public void TestLoggers_03()
        {
            var guid = Guid.NewGuid().ToString();

            var attribute = new BusinessRuleAttribute(guid, RuleLifeCycle.PreCondition, CriticityLevel.Error,
                typeof (ConsoleHandler));

            Assert.IsTrue(attribute.Description == guid);
            Assert.IsTrue(attribute.RuleLifeCycle == RuleLifeCycle.PreCondition);
            Assert.IsTrue(attribute.HandlerType != null);
        }
    }
}