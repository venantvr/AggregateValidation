using System.Linq;
using BusinessRules.Engine;
using BusinessRules.Engine.Handlers;
using BusinessRules.Engine.Providers;
using BusinessRules.Tests.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessRules.Tests
{
    [TestClass]
    public class CrossCuttingConcernsTest
    {
        [TestMethod]
        public void TestContainerThreadSafety()
        {
            var entity = new Entity_04();

            Enumerable.Range(0, 100).AsParallel().ForAll(
                i =>
                {
                    var container = new BusinessRuleContainer
                        <AppDomainProvider<Entity_04>, HandlersProvider<Entity_04>, Entity_04>(
                        entity, () => new ConsoleHandler());

                    Assert.IsTrue(container.Rules.Count() == 2);
                });
        }
    }
}