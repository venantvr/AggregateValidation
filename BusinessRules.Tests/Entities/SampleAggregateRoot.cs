namespace BusinessRules.Tests.Entities
{
    public class SampleAggregateRoot
    {
        public int Id { get; set; }
        public decimal Price { get; set; }

        public void Process()
        {
            Price = Price + 1.0M;
        }
    }
}