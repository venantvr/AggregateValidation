﻿using System;
using System.Collections.Generic;

namespace BusinessRules.Tools
{
    public static class Extensions
    {
        //public static decimal ArrondiEmprunteur(decimal input)
        //{
        //    var output = Math.Round(input, 2, MidpointRounding.AwayFromZero);

        //    return output;
        //}

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var element in source)
                action(element);
        }
    }
}