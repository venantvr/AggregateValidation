namespace BusinessRules.Tools
{
    public interface IBusinessTransaction
    {
        void Commit();

        void Rollback();
    }
}