namespace BusinessRules.Enums
{
    public enum RuleLifeCycle
    {
        PreCondition,
        PostCondition,
        Invariant
    }
}