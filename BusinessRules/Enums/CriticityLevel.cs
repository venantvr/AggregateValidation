﻿namespace BusinessRules.Enums
{
    public enum CriticityLevel
    {
        Ok,
        Warning,
        Error
    }
}