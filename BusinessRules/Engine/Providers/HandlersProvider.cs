using System;
using System.Collections.Generic;
using System.Linq;
using BusinessRules.Contracts;
using BusinessRules.Engine.Template;
using BusinessRules.Template;

namespace BusinessRules.Engine.Providers
{
    public class HandlersProvider<TAggregateRoot> : IHandlersProvider<TAggregateRoot> // where TAggregateRoot : new()
    {
        private Lazy<List<Type>> _loggers;
        private List<AbstractBusinessRule<TAggregateRoot>> _rules;

        public List<Handler> Resolve(HandlersFactory factories)
        {
            return _loggers.Value.Distinct()
                .Select(type => factories.GetFactory(type).Invoke()).ToList();
        }

        public IHandlersProvider<TAggregateRoot> Load(List<AbstractBusinessRule<TAggregateRoot>> rules)
        {
            _rules = rules;

            _loggers = new Lazy<List<Type>>(() =>
                                            {
                                                var loggers = new List<Type>();

                                                foreach (var method in _rules.SelectMany(m => m.GetType().GetMethods()))
                                                {
                                                    var logger = from attribute in method.GetCustomAttributes(true).OfType<BusinessRuleAttribute>()
                                                        select attribute.HandlerType;

                                                    loggers.AddRange(logger);
                                                }

                                                return loggers;
                                            });

            return this;
        }

        public List<Handler> Resolve<THandler>(Func<THandler>[] factories) where THandler : Handler
        {
            return factories.Distinct()
                .Select(type => type.Invoke()).Cast<Handler>().ToList();
        }

        public void Dispose()
        {
        }
    }
}