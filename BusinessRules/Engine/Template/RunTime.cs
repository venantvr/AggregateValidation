﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessRules.Engine.Messages;
using BusinessRules.Enums;
using BusinessRules.Template;

namespace BusinessRules.Engine.Template
{
    internal static class RunTime
    {
        public static List<Issue> ExecuteTest<TAggregateRoot>(this AbstractBusinessRule<TAggregateRoot> what, TAggregateRoot[] aggregateRoots)
        {
            var output = new List<Issue>();

            Func<CriticityLevel, string, string, MessageBase> levelizer = (level, id, rule) =>
                                                                          {
                                                                              switch (level)
                                                                              {
                                                                                  case CriticityLevel.Error:
                                                                                      return new Error(id, rule);
                                                                                  case CriticityLevel.Warning:
                                                                                      return new Warning(id, rule);
                                                                                  default:
                                                                                      return new Information(id, rule);
                                                                              }
                                                                          };

            foreach (var method in what.GetType().GetMethods())
            {
                try
                {
                    var messages = from attribute in method.GetCustomAttributes(true).OfType<BusinessRuleAttribute>()
                        let parameters = method.GetParameters()
                        let level = attribute.CriticityLevel
                        // CriticityLevel.Warning
                        let handler = attribute.HandlerType
                        let ok =
                            parameters.Length == 0
                                ? (bool) method.Invoke(what, null)
                                : (bool) method.Invoke(what, aggregateRoots.AsQueryable().Cast<object>().ToArray())
                        where !ok
                        // TODO : notifiers
                        select
                            new Issue
                            {
                                Message =
                                    levelizer(level, what.Key, string.Format(attribute.Description, what.Key)),
                                HandlerType = handler
                            };

                    output.AddRange(messages);
                }
                catch (Exception ex)
                {
                    var issue = new Issue
                                {
                                    Message = levelizer(CriticityLevel.Error, what.Key, string.Format(ex.ToString(), what.Key)),
                                    HandlerType = null // TODO
                                };

                    output.Add(issue);
                }
            }

            return output;
        }
    }
}