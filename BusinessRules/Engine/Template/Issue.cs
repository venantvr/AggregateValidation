using System;
using BusinessRules.Template;

namespace BusinessRules.Engine.Template
{
    internal class Issue
    {
        public Type HandlerType;
        public MessageBase Message;
    }
}