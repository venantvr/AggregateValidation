﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using BusinessRules.Enums;
using BusinessRules.Template;

namespace BusinessRules.Engine.Template
{
    public abstract class Handler : IDisposable
    {
        //public Notifier(List<MessageBase> messages)
        //{
        //    _messages = messages;
        //}

        private readonly List<MessageBase> _messages = new List<MessageBase>();

        public IReadOnlyCollection<MessageBase> Messages => _messages.AsReadOnly();

        public void Dispose()
        {
            Commit();

            _messages.Clear();
        }

        public abstract void Commit();

        public abstract void DefaultAction();

        public static Handler operator +(Handler handler, MessageBase message)
        {
            return CallContextHelper.Concat(handler, new List<MessageBase> { message });
        }

        public static Handler operator +(Handler handler, Exception message)
        {
            var messageBase = new MessageBase(message.ToString(), CriticityLevel.Error);

            return CallContextHelper.Concat(handler, new List<MessageBase> { messageBase });
        }

        public static Handler operator +(Handler handler, List<MessageBase> messages)
        {
            return CallContextHelper.Concat(handler, messages);
        }

        private static class CallContextHelper
        {
            [ThreadStatic] private static Guid? _uid;

            private static string Uid
            {
                get
                {
                    if (_uid == null)
                        _uid = Guid.NewGuid();

                    return _uid.ToString();
                }
            }

            private static int Count()
            {
                if (CallContext.GetData(Uid) == null)
                {
                    CallContext.SetData(Uid, 0);
                }

                var cnt = (int) CallContext.GetData(Uid);

                return cnt;
            }

            // A rendre thread-safe ?? => NON, car appartient à chaque thread en cours
            private static void Trace()
            {
                var cnt = Count() + 1;

                // ReSharper disable once RedundantAssignment
                CallContext.SetData(Uid, cnt);
            }

            public static Handler Concat(Handler handler, List<MessageBase> messages)
            {
                handler._messages.AddRange(messages);

                handler.DefaultAction();

                if (messages != null)
                {
                    Trace();
                }

                return handler;
            }
        }
    }
}