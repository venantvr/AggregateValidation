﻿using System;
using System.Linq;
using BusinessRules.Contracts;
using BusinessRules.Engine.Logger;
using BusinessRules.Engine.Template;

namespace BusinessRules.Engine
{
    public class BusinessRuleEngine<TRules, THandlers, TAggregateRoot> : IDisposable
        where TRules : IRulesProvider<TAggregateRoot>, new()
        where THandlers : IHandlersProvider<TAggregateRoot>, new()
    {
        private readonly BusinessRuleContainer<TRules, THandlers, TAggregateRoot> _container;

        [Obsolete]
        public BusinessRuleEngine(params Func<Handler>[] factories)
        {
            _container =
                new BusinessRuleContainer<TRules, THandlers, TAggregateRoot>(factories);
        }

        public BusinessRuleEngine(TAggregateRoot aggregateRoot, params Func<Handler>[] factories)
        {
            _container =
                new BusinessRuleContainer<TRules, THandlers, TAggregateRoot>(aggregateRoot, factories);
        }

        public void Dispose()
        {
            _container.Dispose();
        }

        public void Verify(TAggregateRoot aggregateRoot)
        {
            using (new LoggerOptions(_container.Handlers))
            {
                foreach (var rule in _container.RulesApplyingTo(aggregateRoot))
                {
#pragma warning disable 612
                    rule.Load(aggregateRoot).Verify();
#pragma warning restore 612
                    //rule.Verify(aggregateRoot);
                }
            }
        }

        public void Verify(TAggregateRoot aggregateRoot, params Func<Handler>[] factories)
        {
            var instances = factories.Select(f => f.Invoke()).ToList();

            using (new LoggerOptions(_container.Handlers.Union(instances).ToList().AsReadOnly()))
            {
                foreach (var rule in _container.RulesApplyingTo(aggregateRoot))
                {
#pragma warning disable 612
                    rule.Load(aggregateRoot).Verify();
#pragma warning restore 612
                    rule.Verify(aggregateRoot, instances);
                }
            }
        }
    }
}