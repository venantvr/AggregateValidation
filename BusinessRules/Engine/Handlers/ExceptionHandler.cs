﻿using System;
using System.Linq;
using BusinessRules.Engine.Template;
using BusinessRules.Tools;

namespace BusinessRules.Engine.Handlers
{
    public class ExceptionHandler : Handler
    {
        public override void Commit()
        {
            DefaultAction();
        }

        public override void DefaultAction()
        {
            if (!Messages.Any()) return;

            var exceptions = Messages.Select(message => new BusinessException(message)).ToList();

            throw new AggregateException(exceptions);
        }
    }
}