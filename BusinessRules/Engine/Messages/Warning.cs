﻿using System;
using BusinessRules.Enums;
using BusinessRules.Template;

namespace BusinessRules.Engine.Messages
{
    internal class Warning : MessageBase, IEquatable<Warning>
    {
        public Warning()
            : base(@"", CriticityLevel.Warning)
        {
        }

        public Warning(string id, string rule)
            : base(rule, CriticityLevel.Warning)
        {
            Id = id;
            Rule = rule;
        }

        public string Id { get; }

        public string Rule { get; }

        public bool Equals(Warning other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && string.Equals(Rule, other.Rule);
        }

        public override bool Equals(object other)
        {
            var obj = other as Warning;
            return obj != null && Id == obj.Id && Rule == obj.Rule;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode() * 397) ^ (Rule?.GetHashCode() ?? 0);
            }
        }

        //public override int GetHashCode()
        //{
        //    var i = Id ?? 0;
        //    return i.GetHashCode() + Rule.GetHashCode();
        //}

        public new string ToString()
        {
            return $"{Id} - {Rule}";
        }
    }
}