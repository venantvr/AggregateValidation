using System;
using BusinessRules.Enums;
using BusinessRules.Template;

namespace BusinessRules.Engine.Messages
{
    internal class Information : MessageBase, IEquatable<Error>
    {
        public Information(string message, CriticityLevel level) : base(message, level)
        {
        }

        public Information(string id, string rule)
            : base(rule, CriticityLevel.Error)
        {
            Id = id;
            Rule = rule;
        }

        public string Id { get; }

        public string Rule { get; }

        public bool Equals(Error other)
        {
            //throw new NotImplementedException();
            return false;
        }
    }
}