﻿using System;
using BusinessRules.Enums;
using BusinessRules.Template;

namespace BusinessRules.Engine.Messages
{
    internal class Error : MessageBase, IEquatable<Error>
    {
        public Error()
            : base(@"", CriticityLevel.Error)
        {
        }

        public Error(string id, string rule)
            : base(rule, CriticityLevel.Error)
        {
            Id = id;
            Rule = rule;
        }

        public string Id { get; }

        public string Rule { get; }

        public bool Equals(Error other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && string.Equals(Rule, other.Rule);
        }

        public override bool Equals(object other)
        {
            var obj = other as Error;
            return obj != null && Id == obj.Id && Rule == obj.Rule;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode() * 397) ^ (Rule?.GetHashCode() ?? 0);
            }
        }

        //public override int GetHashCode()
        //{
        //    var i = Id ?? 0;
        //    return i.GetHashCode() + Rule.GetHashCode();
        //}

        public new string ToString()
        {
            return $"{Id} - {Rule}";
        }
    }
}